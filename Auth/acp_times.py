"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

fastest_times = {
	"1":(200,34),
	"2":(200,32),
	"3":(200,30),
	"4":(400,28),
	"5":(300,26)
}

slowest_times = {
	"1":(0,15),
	"2":(200,15),
	"3":(200,15),
	"4":(200,11.428),
	"5":(400,13.333)
}

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    initial_start = arrow.get(brevet_start_time, "YYYY-MM-DD HH:mm")

    if int(brevet_dist_km) <= 200:
        rate = fastest_times["1"][1]
        new_time = initial_start.shift(hours = control_dist_km/rate)

    elif int(brevet_dist_km) > 200 and control_dist_km <= 400:
        rate = fastest_times["2"][1] 
        new_time = initial_start.shift(hours = control_dist_km/rate)

    elif int(brevet_dist_km) > 400 and control_dist_km <= 600:
        rate = fastest_times["3"][1]
        new_time = initial_start.shift(hours = control_dist_km/rate)

    elif int(brevet_dist_km) > 600 and control_dist_km <= 1000:
        rate = fastest_times["4"][1]
        new_time = initial_start.shift(hours = control_dist_km/rate)

    elif int(brevet_dist_km) > 1000 and control_dist_km <= 1300:
        rate = fastest_times["5"][1]
        new_time = initial_start.shift(hours = control_dist_km/rate)

    return new_time.isoformat()

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    initial_start = arrow.get(brevet_start_time, "YYYY-MM-DD HH:mm")
    
    if int(brevet_dist_km) <= 200:
        rate = slowest_times["1"][1]
        new_time = initial_start.shift(hours = control_dist_km/rate)

    elif int(brevet_dist_km) > 200 and control_dist_km <= 400:
        rate = slowest_times["2"][1]
        new_time = initial_start.shift(hours = control_dist_km/rate)

    elif int(brevet_dist_km) > 400 and control_dist_km <= 600:
        rate = slowest_times["3"][1]
        new_time = initial_start.shift(hours = control_dist_km/rate)

    elif int(brevet_dist_km) > 600 and control_dist_km <= 1000:
        rate = slowest_times["4"][1]
        new_time = initial_start.shift(hours = control_dist_km/rate)

    elif int(brevet_dist_km) > 1000 and control_dist_km <= 1300:
        rate = slowest_times["5"][1]
        new_time = initial_start.shift(hours = control_dist_km/rate)

    return new_time.isoformat()
