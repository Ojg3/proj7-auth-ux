# Laptop Service
import flask_brevets
import acp_times
import config
from flask import Flask
from flask_restful import Resource, Api
import pymongo
from pymongo import MongoClient
import os
import logging


# Instantiate the app
app = Flask(__name__)
api = Api(app)
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.caldb

class ListAll(Resource):
    def get(self):
        data = db.tododb.find()
        open = []
        close = []
        for item in data:
            open.append({'open_times':item['open_time']})
            close.append({'close_times':item['close_time']})
        return {
            'open':[open]
            'close':[close]
        }

class ListOpen(Resource):
    def get(self):
        data = db.tododb.find()
        open = []
        items = [item for item in data]:
            open.append(item['open_time'])
        return {
            'open': [open]
        }

class ListClose(Resource):
    def get(self):
        data = db.tododb.find()
        close = []
        items = [item for item in data]:
            close.append(item['close_time'])
        return {
            'close' : [close]
        }

# Create routes
# Another way, without decorators
#api.add_resource(Laptop, '/')
api.add_resource(ListAll, '/')
# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
