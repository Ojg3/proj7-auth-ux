Author: Otis Greer
Email: ogreer@uoregon.edu
Description:
Though the program does not work in its entirety, the program can still take a user to the localHost:5000 port  and have them enter times into the slots provided.Once that is complete, the user can click Submit which will enter the values into a database. The user can click the Display button to display all the times, or in the URL the user can type listOpenOnly, listCloseOnly, and listAll which will return the desired set of times. The login key will only work if the user types in login after the 5000/ in the URL. The program should be able to log a user in and remember them so that if they return, they would not have to sign in again.
